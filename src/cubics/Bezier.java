package cubics;

import model3D.Solid;
import transforms.Cubic;
import transforms.Point3D;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class Bezier extends Solid {
    public Bezier(double param)
    {
        Point3D p1 = new Point3D(0,0, 0);
        Point3D p2 = new Point3D(1, 2, 0);
        Point3D p3 = new Point3D(2, -2, 1);
        Point3D p4 = new Point3D(2.5, 3, 2);
        Cubic bezier1 = new Cubic(Cubic.BEZIER, p1, p2, p3, p4);

        Point3D p5 = new Point3D(2,1, 0);
        Point3D p6 = new Point3D(1, 3, -1);
        Point3D p7 = new Point3D(2, -2, -2);
        Point3D p8 = new Point3D(2.5, 0, 2);
        Cubic bezier2 = new Cubic(Cubic.BEZIER, p5, p6, p7, p8);

        List<Cubic> bezier = new ArrayList<>();
        bezier.add(bezier1);
        bezier.add(bezier2);

        for(Cubic b:bezier) {
            for (double i = 0; i <= 1; i += param) {
                Point3D computed = b.compute(i);
                getVertexBuffer().add(computed);
            }
            for (int i = 1; i < getVertexBuffer().size(); i++) {
                getIndexBuffer().add(i - 1);
                getIndexBuffer().add(i);
            }
        }
    }
}
