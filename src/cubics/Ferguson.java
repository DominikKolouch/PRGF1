package cubics;

import model3D.Solid;
import transforms.Cubic;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Ferguson extends Solid {
    public Ferguson(double param) {
        Point3D p1 = new Point3D(1, 1, 1);
        Point3D p2 = new Point3D(0, -2, 1);
        Point3D p3 = new Point3D(2, 4, 0);
        Point3D p4 = new Point3D(0, 1, 0);
        Cubic ferguson = new Cubic(Cubic.FERGUSON, p1, p2, p3, p4);

        Point3D p5 = new Point3D(1, 2, 4);
        Point3D p6 = new Point3D(0, 3, 1);
        Point3D p7 = new Point3D(-2, 4, 6);
        Cubic ferguson2 = new Cubic(Cubic.FERGUSON, p4, p5, p6, p7);

        List<Cubic> fergus = new ArrayList<>();
        fergus.add(ferguson);
        fergus.add(ferguson2);

        for(Cubic f:fergus) {
            for (double i = 0; i <= 1; i += param) {
                Point3D computed = f.compute(i);
                getVertexBuffer().add(computed);
            }
            for (int i = 1; i < getVertexBuffer().size(); i++) {
                getIndexBuffer().add(i - 1);
                getIndexBuffer().add(i);
            }
        }

    }
}
