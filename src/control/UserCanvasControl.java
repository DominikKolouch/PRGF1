package control;

import cubics.Bezier;
import cubics.Ferguson;
import model3D.Axis;
import model3D.Cube;
import model3D.Pyramid;
import model3D.Solid;
import raster.Raster;
import rasterize.BufferedImageRasterizer;
import render.EdgeRenderer;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class UserCanvasControl implements MouseListener, KeyListener, MouseMotionListener, ComponentListener {


    private final JPanel panel;
    private final Raster raster;

    private final BufferedImageRasterizer lineRasterizer;

    private Mat4 cubeModel = new Mat4Identity();
    private Mat4 pyramidModel = new Mat4Identity();
    private final Mat4 axisModel = new Mat4Identity();
    private Mat4 curvesModel = new Mat4Identity();

    private final Solid cube = new Cube();
    private boolean cubeActive = true;
    private final Solid pyramid = new Pyramid();
    private boolean pyramidActive = false;

    private final Solid bezier = new Bezier(0.01);
    private final Solid ferguson = new Ferguson(0.01);

    private boolean modelControl = false;

    private Point mousePos;
    private Camera camera;

    private final EdgeRenderer cubeRenderer;
    private final EdgeRenderer axisRenderer;
    private final EdgeRenderer pyramidRenderer;
    private final EdgeRenderer curvesRenderer;

    private Thread rotationThread;
    private boolean threadActive = false;

    private Mat4 viewMatrix = new Mat4Identity();

    private int MODEL_CONST = 0; //0 - cube, 1 - pyramid, 2 - both
    private int ROT_CONST = 0; //0 - vlastní bod, 1 - X, 2 - Y, 3 - Z

    private boolean persp = true;

    private String activSolidName = "";
    private String activeRotAxisName = "";


    public UserCanvasControl(JPanel panel, Raster raster) {
        this.panel = panel;
        this.raster = raster;
        lineRasterizer = new BufferedImageRasterizer(raster);

        cubeRenderer = new EdgeRenderer(raster);
        axisRenderer = new EdgeRenderer(raster);
        pyramidRenderer = new EdgeRenderer(raster);
        curvesRenderer = new EdgeRenderer(raster);

        cubeRenderer.setRasterizer(lineRasterizer);
        axisRenderer.setRasterizer(lineRasterizer);
        pyramidRenderer.setRasterizer(lineRasterizer);
        curvesRenderer.setRasterizer(lineRasterizer);


        clear();
        panel.repaint();
        camera = new Camera()
                .withPosition(new Vec3D(5, 0, 0))
                .withAzimuth(Math.PI)
                .withZenith(0)
                .withFirstPerson(false);
    }

    @Override
    public void keyPressed(KeyEvent e) {

        double step = 0.1;

        switch (e.getKeyCode()) {
            case KeyEvent.VK_C -> modelControl = !modelControl;
            case KeyEvent.VK_A -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(0.1, 0, 0));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(0.1, 0, 0));
                } else camera = camera.left(step);
            }
            case KeyEvent.VK_D -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(-0.1, 0, 0));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(-0.1, 0, 0));
                } else camera = camera.right(step);
            }
            case KeyEvent.VK_W -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(0, 0, 0.1));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(0, 0, 0.1));
                } else camera = camera.up(step);
            }
            case KeyEvent.VK_S -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(0, 0, -0.1));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(0, 0, -0.1));
                } else camera = camera.down(step);
            }
            case KeyEvent.VK_Q -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(0, 0.1, 0));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(0, 0.1, 0));
                } else camera = camera.forward(step);
            }
            case KeyEvent.VK_E -> {
                if (modelControl) {
                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4Transl(0, -0.1, 0));
                    if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Transl(0, -0.1, 0));
                } else camera = camera.backward(step);
            }
            case 109 -> //+
                    shrinkActiveModel();
            case 107 -> //-
                    expandActiveModel();

            case KeyEvent.VK_P -> persp = !persp;
            case KeyEvent.VK_R -> {
                threadActive = !threadActive;
                rotationThread = new Thread(() -> {
                    while (threadActive) {
                            switch (ROT_CONST) {
                                case 0 -> {
                                    if (cubeActive)
                                        cubeModel = cubeModel.mul(new Mat4Rot(0.1, cube.getVertexBuffer().get(1).getX(), cube.getVertexBuffer().get(2).getY(), cube.getVertexBuffer().get(3).getZ()));
                                    if (pyramidActive)
                                        pyramidModel = pyramidModel.mul(new Mat4Rot(0.1, pyramid.getVertexBuffer().get(0).getX(), pyramid.getVertexBuffer().get(0).getY(), pyramid.getVertexBuffer().get(0).getZ()));
                                    activeRotAxisName = "Point rotation";
                                }
                                case 1 -> {
                                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4RotX(0.1));
                                    if (pyramidActive) pyramidModel = pyramidModel.mul((new Mat4RotX(0.1)));
                                    activeRotAxisName = "X rotation";
                                }
                                case 2 -> {
                                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4RotY(0.1));
                                    if (pyramidActive) pyramidModel = pyramidModel.mul((new Mat4RotY(0.1)));
                                    activeRotAxisName = "Y rotation";
                                }
                                case 3 -> {
                                    if (cubeActive) cubeModel = cubeModel.mul(new Mat4RotZ(0.1));
                                    if (pyramidActive) pyramidModel = pyramidModel.mul((new Mat4RotZ(0.1)));
                                    activeRotAxisName = "Z rotation";
                                }
                            }

                        clear();
                        reDraw();
                        try {
                            Thread.sleep(17);           //60 snímků za sekundu
                        } catch (InterruptedException ex) {
                        }
                    }
                });
                rotationThread.start();
            }
            case KeyEvent.VK_M -> {
                activateModels(MODEL_CONST);
                MODEL_CONST++;
            }
            case KeyEvent.VK_N -> ROT_CONST = (ROT_CONST + 1) % 4;
            case KeyEvent.VK_I -> {
                String info = """
                        ovládání kamery QWEASD
                        Přepnutí z kontroly kamery na kontrolu objektu: c -> Ovladaní objektu: WASD
                        Změna pohledu kamery: p
                        Výběr aktivního objektu: m
                        Zvětšení/zmenšení aktivního objektu: +/-
                        Aktivace animace: r
                        Změna rotace objektů: n""";
                JOptionPane.showMessageDialog(null, info, "Informace", JOptionPane.INFORMATION_MESSAGE);
            }

        }
        clear();
        reDraw();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePos = e.getPoint();
    }


    @Override
    public void mouseDragged(MouseEvent e) {

        double azimut = Math.PI * ((float) (mousePos.x - e.getX()) / raster.getWidth());
        double zenit = Math.PI * ((float) (mousePos.y - e.getY()) / raster.getHeight());

        if (zenit > 90) zenit = 90;
        else if (zenit < -90) zenit = -90;
        if (azimut > 90) azimut = 90;
        else if (azimut < -90) azimut = -90;

        camera = camera.addAzimuth(azimut);
        camera = camera.addZenith(zenit);
        clear();
        reDraw();
        mousePos = e.getPoint();
    }

    @Override
    public void componentResized(ComponentEvent e) {
        raster.resize(panel);
        clear();
        reDraw();
    }

    public void reDraw() {

        drawString();
        viewMatrix = camera.getViewMatrix();

        renderObjects();
        panel.repaint();
    }


    private void clear() {
        raster.clear();
    }

    private void renderObjects() {
        pyramidRenderer.setModel(pyramidModel);
        pyramidRenderer.setView(viewMatrix);

        cubeRenderer.setModel(cubeModel);
        cubeRenderer.setView(viewMatrix);

        axisRenderer.setModel(axisModel);
        axisRenderer.setView(viewMatrix);

        curvesRenderer.setModel(curvesModel);
        curvesRenderer.setView(viewMatrix);

        Axis axisX = new Axis().getXAxis();
        Axis axisY = new Axis().getYAxis();
        Axis axisZ = new Axis().getZAxis();
        Mat4 projection = new Mat4Identity();

        if (persp) {

            projection = new Mat4PerspRH(20, (float) raster.getHeight() / raster.getWidth(), 0.5, 50);

        } else {
            projection = new Mat4OrthoRH(6, 4, 0.1, 50);

        }



        cubeRenderer.setProjection(projection);
        pyramidRenderer.setProjection(projection);
        axisRenderer.setProjection(projection);
        curvesRenderer.setProjection(projection);

        curvesRenderer.render(bezier,0xffffff);
        curvesRenderer.render(ferguson,0x00fff0);


        cubeRenderer.render(cube);
        pyramidRenderer.render(pyramid, 0xFF0087);
        axisRenderer.render(axisX, 0xff0000);
        axisRenderer.render(axisY, 0x00ff00);
        axisRenderer.render(axisZ, 0x0000ff);
    }


    private void expandActiveModel() {
        if (cubeActive) cubeModel = cubeModel.mul(new Mat4Scale(1.01));
        if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Scale(1.01));
    }

    private void shrinkActiveModel() {
        if (cubeActive) cubeModel = cubeModel.mul(new Mat4Scale(0.99));
        if (pyramidActive) pyramidModel = pyramidModel.mul(new Mat4Scale(0.99));
    }

    private void activateModels(int CONST) {
        switch (CONST % 3) {

            case 0 -> {
                cubeActive = true;
                pyramidActive = false;
                activSolidName = "Cube";
            }
            case 1 -> {
                cubeActive = false;
                pyramidActive = true;
                activSolidName = "Pyramid";
            }
            case 2 -> {
                cubeActive = true;
                pyramidActive = true;
                activSolidName = "Both";
            }
        }
    }

    private void drawString() {
        raster.drawString("Info: i", 5, 10);
        raster.drawString("Active model: " + activSolidName, 5, 22);
        raster.drawString("Rotation: " + activeRotAxisName, 5, 34);
        raster.drawString("Model control: " + modelControl, 5, 46);
    }


    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }
}

