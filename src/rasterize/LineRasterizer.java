package rasterize;

import raster.Raster;

import java.awt.*;

public abstract class LineRasterizer {

    /*
     * abstraktní třída pro vykreslení čáry v rasteru
     * souřadnice, Line, Points
     */

    protected Raster raster;

    public void rasterize(int x1, int y1, int x2, int y2) {
        rasterize(x1, y1, x2, y2, 0xffffff);
    }

    public void rasterize(int x1, int y1, int x2, int y2, int color) {
    }

    public void rasterize(Point startPoint, Point endPoint, int color) {
        rasterize(startPoint.x, startPoint.y, endPoint.x, endPoint.y, color);
    }


    public void rasterize(int x1, int y1, int x2, int y2, int space, int color) {

    }

    public void rasterize(Point startPoint, Point endPoint, int space, int color) {
        rasterize(startPoint.x, startPoint.y, endPoint.x, endPoint.y, space, color);
    }


}
