package render;

import model3D.Solid;
import raster.Raster;
import rasterize.LineRasterizer;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Point3D;
import transforms.Vec3D;

import java.util.ArrayList;
import java.util.List;

public class EdgeRenderer extends Renderer {

    Raster raster;
    private LineRasterizer rasterizer;
    private Mat4 model = new Mat4Identity(), view = new Mat4Identity(), proj = new Mat4Identity();

    public EdgeRenderer() {

    }

    public EdgeRenderer(Raster raster) {
        this.raster = raster;
    }


    @Override
    public void setModel(Mat4 model) {
        this.model = model;
    }

    @Override
    public void setView(Mat4 view) {
        this.view = view;
    }

    @Override
    public void setProjection(Mat4 proj) {
        this.proj = proj;
    }

    @Override
    public void render(Solid solid, int color) {


        List<Point3D> tempVB = new ArrayList<>();
        Mat4 trans = model.mul(view).mul(proj);
        for (Point3D point : solid.getVertexBuffer()) {

            point = point.mul(trans);
            tempVB.add(point);

        }


        Point3D a, b;
        for (int i = 0; i < solid.getIndexBuffer().size() - 1; ++i) {
            int indexA = solid.getIndexBuffer().get(i);
            int indexB = solid.getIndexBuffer().get(i + 1);
            a = tempVB.get(indexA);
            b = tempVB.get(indexB);

            if (clip(a) || clip(b)) continue;

            render(a, b, color);
        }

    }


    @Override
    public void setRasterizer(LineRasterizer rasterizer) {
        this.rasterizer = rasterizer;
    }

    public void render(Point3D start, Point3D end, int color) {


        //dehomog
        Vec3D startVec = new Vec3D();
        Vec3D endVec = new Vec3D();

        if (start.dehomog().isPresent() && end.dehomog().isPresent()) {
            startVec = start.dehomog().get();
            endVec = end.dehomog().get();
        } else return;

        //3d->2d
        //viewport


        int x1 = (int) (raster.getWidth() / 2 * (1 + startVec.getX()));
        int y1 = (int) (raster.getHeight() / 2 * (1 - startVec.getY()));

        int x2 = (int) (raster.getWidth() / 2 * (1 + endVec.getX()));
        int y2 = (int) (raster.getHeight() / 2 * (1 - endVec.getY()));

        //drawLine
        rasterizer.rasterize(x1, y1, x2, y2, color);
    }


    private boolean clip(Point3D point3D) {
        double X = point3D.getX();
        double Y = point3D.getY();
        double Z = point3D.getZ();
        double W = point3D.getW();

        return !(-W <= X && X <= W && -W <= Y && Y <= W && Z >= 0 && Z <= W);
    }
}
