package render;

import model3D.Solid;
import rasterize.LineRasterizer;
import transforms.Mat4;

public abstract class Renderer {


    public void setModel(Mat4 model) {
    }

    public void setView(Mat4 view) {
    }

    public void setProjection(Mat4 proj) {
    }

    public void render(Solid solid) {
        render(solid, 0xffff00);
    }

    public void render(Solid solid, int color) {
    }

    public void setRasterizer(LineRasterizer rasterizer) {
    }
}
