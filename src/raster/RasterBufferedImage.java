package raster;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class RasterBufferedImage implements Raster {
    public static int TYPE_INT_RGB = BufferedImage.TYPE_INT_RGB;
    int backgroundColor;
    private BufferedImage img;

    public RasterBufferedImage(int width, int height) {
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        backgroundColor = img.getRGB(0, 0);
    }

    public RasterBufferedImage(BufferedImage img) {
        this.img = img;
    }

    @Override
    public void setPixel(int x, int y, int color) {
        if (validateCoord(x, y)) {
            img.setRGB(x, y, color);
        }
    }

    @Override
    public int getPixel(int x, int y) {
        if (validateCoord(x, y)) {
            return img.getRGB(x, y);
        }
        return backgroundColor;
    }

    @Override
    public int getWidth() {
        return img.getWidth();
    }

    @Override
    public int getHeight() {
        return img.getHeight();
    }

    public void clear() {
        Graphics gr = img.getGraphics();
        gr.setColor(new Color(0x2f2f2f));
        gr.fillRect(0, 0, img.getWidth(), img.getHeight());
    }

    @Override
    public void drawString(String word, int x, int y) {

        img.getGraphics().drawString(word, x, y);
    }

    @Override
    public void resize(JPanel panel) {
        BufferedImage newImg = new BufferedImage(panel.getWidth(), panel.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        img = newImg;
    }

    public Graphics getGraphics() {
        return img.getGraphics();
    }

    public void present(Graphics graphics) {
        graphics.drawImage(img, 0, 0, null);
    }

    public BufferedImage getImg() {
        return img;
    }

    private boolean validateCoord(int x, int y) {
        return (x < getWidth() && x >= 0 && y < getHeight() && y >= 0);
    }
}
