package raster;


import javax.swing.*;

public interface Raster {
    void setPixel(int x, int y, int color);

    int getPixel(int x, int y);

    int getWidth();

    int getHeight();

    void clear();

    void drawString(String word, int x, int y);

    void resize(JPanel panel);
}
