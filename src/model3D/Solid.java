package model3D;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Solid {
    private final List<Integer> indexBuffer;
    private final List<Point3D> vertexBuffer;


    public Solid() {
        indexBuffer = new ArrayList<>();
        vertexBuffer = new ArrayList<>();
    }

    public List<Point3D> getVertexBuffer() {
        return vertexBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }
}
