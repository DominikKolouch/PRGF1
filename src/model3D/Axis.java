package model3D;

import transforms.Point3D;

public class Axis extends Solid {
    public Axis() {

    }

    public Axis getXAxis() {
        getVertexBuffer().add(new Point3D(0, 0, 0));
        getVertexBuffer().add(new Point3D(2, 0, 0));

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        return this;
    }

    public Axis getYAxis() {
        getVertexBuffer().add(new Point3D(0, 0, 0));
        getVertexBuffer().add(new Point3D(0, 2, 0));

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        return this;
    }

    public Axis getZAxis() {
        getVertexBuffer().add(new Point3D(0, 0, 0));
        getVertexBuffer().add(new Point3D(0, 0, 2));

        getIndexBuffer().add(0);
        getIndexBuffer().add(1);

        return this;
    }

}
