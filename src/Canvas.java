import control.UserCanvasControl;
import raster.Raster;
import raster.RasterBufferedImage;

import javax.swing.*;
import java.awt.*;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2020
 */

public class Canvas {

    private static UserCanvasControl userCanvasControl;
    private final JFrame frame;
    private final JPanel panel;
    private final Raster raster;

    //nastavení Canvas
    public Canvas(int width, int height) {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //vytvoření nového rasteru
        raster = new RasterBufferedImage(800, 600);

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                ((RasterBufferedImage) raster).present(g);

            }
        };
        userCanvasControl = new UserCanvasControl(panel, raster);

        panel.setPreferredSize(new Dimension(width, height));
        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);


        panel.setFocusable(true);
        panel.requestFocusInWindow();

        panel.addMouseListener(userCanvasControl);
        panel.addMouseMotionListener(userCanvasControl);
        panel.addKeyListener(userCanvasControl);
        panel.addComponentListener(userCanvasControl);
        userCanvasControl.reDraw();

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Canvas(800, 600));
    }
}